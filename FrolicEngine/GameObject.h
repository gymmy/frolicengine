#pragma once
#include "Object.h"
#include "Component.h"
#include "Transform.h"
class Component;
class Transform;

class GameObject : public Object {
protected:
	//properties
	std::string name;
	std::list<Component*> components;
	Transform* transform;
public:
	//constructors
	explicit GameObject();
	explicit GameObject(std::string p_name);
	explicit GameObject(std::string p_name, Vector3 p_position);
	explicit GameObject(std::string p_name, Vector3 p_position, Vector3 p_rotation);
	explicit GameObject(std::string p_name, Vector3 p_position, Vector3 p_rotation, Vector3 p_scale);
	//copy constructor
	GameObject(const GameObject& go);
	//copy assignment operator
	GameObject& operator=(const GameObject& go);
	//destructor
	~GameObject();
	void AddComponent(Component* p_component);
	void RemoveComponent(Component p_component);
	Component *GetComponent(Component p_component);
	inline std::string GetName(){return this->name;}

	void SetName(std::string p_name) { this->name = p_name;}
	inline Transform GetTransform() { return *transform; }
	inline Vector3 GetLocation() { return transform->Location; }
	inline Quaternion GetRotation() { return  transform->Rotation; }
	inline Vector3 GetScale() { return  transform->Scale; }
	void SetLocation(float p_x, float p_y, float p_z);
	void SetRotation(float p_roll, float p_pitch, float p_yaw);
	void SetScale(float p_x, float p_y, float p_z);
};