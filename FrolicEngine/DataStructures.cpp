#include "DataStructures.h"

Vector2::Vector2() {
	X = 0;
	Y = 0;
}

Vector2::Vector2(float p_x, float p_y) {
	X = p_x;
	Y = p_y;
}

Vector2::~Vector2() {

}

Vector2 Vector2::operator+(const Vector2 p_rhs) {
	X += p_rhs.X;
	Y += p_rhs.Y;
	return *this;
}

Vector2 Vector2::operator+=(const Vector2 p_rhs) {
	X += p_rhs.X;
	Y += p_rhs.Y;
	return *this;
}

Vector2 Vector2::operator-(const Vector2 p_rhs) {
	X -= p_rhs.X;
	Y -= p_rhs.Y;
	return *this;
}

Vector2 Vector2::operator-=(const Vector2 p_rhs) {
	X -= p_rhs.X;
	Y -= p_rhs.Y;
	return *this;
}

Vector2 Vector2::operator*(const float p_rhs) {
	X *= p_rhs;
	Y *= p_rhs;
	return *this;
}

Vector2 Vector2::operator*=(const float p_rhs) {
	X *= p_rhs;
	Y *= p_rhs;
	return *this;
}

Vector2 Vector2::operator/(const float p_rhs) {
	X /= p_rhs;
	Y /= p_rhs;
	return *this;
}

Vector2 Vector2::operator/=(const float p_rhs) {
	X /= p_rhs;
	Y /= p_rhs;
	return *this;
}

Vector2 Vector2::operator=(const Vector2 p_rhs) {
	//	Identity Check
	if (this == &p_rhs)
		return *this;
	X = p_rhs.X;
	Y = p_rhs.Y;
	return *this;
}

Vector3::Vector3() {
	X = 0;
	Y = 0;
	Z = 0;
}

Vector3::Vector3(float p_x, float p_y, float p_z) {
	X = p_x;
	Y = p_y;
	Z = p_z;
}

Vector3::~Vector3() {

}

Vector3 Vector3::operator+(const Vector3 p_rhs) {
	X += p_rhs.X;
	Y += p_rhs.Y;
	Z += p_rhs.Z;
	return *this;
}

Vector3 Vector3::operator+=(const Vector3 p_rhs) {
	X += p_rhs.X;
	Y += p_rhs.Y;
	Z += p_rhs.Z;
	return *this;
}

Vector3 Vector3::operator-(const Vector3 p_rhs) {
	X -= p_rhs.X;
	Y -= p_rhs.Y;
	Z -= p_rhs.Z;
	return *this;
}

Vector3 Vector3::operator-=(const Vector3 p_rhs) {
	X -= p_rhs.X;
	Y -= p_rhs.Y;
	Z -= p_rhs.Z;
	return *this;
}

Vector3 Vector3::operator*(const float p_rhs) {
	X *= p_rhs;
	Y *= p_rhs;
	Z *= p_rhs;
	return *this;
}

Vector3 Vector3::operator*=(const float p_rhs) {
	X *= p_rhs;
	Y *= p_rhs;
	Z *= p_rhs;
	return *this;
}

Vector3 Vector3::operator/(const float p_rhs) {
	X /= p_rhs;
	Y /= p_rhs;
	Z /= p_rhs;
	return *this;
}

Vector3 Vector3::operator/=(const float p_rhs) {
	X /= p_rhs;
	Y /= p_rhs;
	Z /= p_rhs;
	return *this;
}

Vector3 Vector3::operator=(const Vector3 p_rhs) {
	//	Identity Check
	if (this == &p_rhs)
		return *this;
	X = p_rhs.X;
	Y = p_rhs.Y;
	Z = p_rhs.Z;
	return *this;
}