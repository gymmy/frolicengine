#include "GameObject.h"

GameObject::GameObject() {
	transform = new Transform();
}

GameObject::GameObject(std::string p_name) {
	this->name = p_name;
	transform = new Transform();
}

GameObject::GameObject(const GameObject& go) {
	std::string _newName = "Copy_";
	_newName.append(go.name);
	SetName(_newName);
	std::cout << "Log: GameObject user defined copy constructor used." << std::endl;
}

GameObject::~GameObject() {
}

void GameObject::AddComponent(Component* p_component) {
	this->components.push_back(p_component);
}

void GameObject::RemoveComponent(Component p_component) {
}

GameObject &GameObject::operator=(const GameObject& go){
	//	Identity check
	if (this == &go)
		return *this;
	std::string _newName = "Copy_";
	_newName.append(go.name);
	SetName(_newName);
	std::cout<<"Log: GameObject user defined copy assignment operator used."<< std::endl;
	return *this;
}

Component *GameObject::GetComponent(Component p_component) {
	return NULL;
}

void GameObject::SetLocation(float p_x, float p_y, float p_z) {
	transform->Location.X = p_x;
	transform->Location.Y = p_y;
	transform->Location.Z = p_z;
}

void GameObject::SetRotation(float p_roll, float p_pitch, float p_yaw) {
	transform->Rotation.Roll = p_roll;
	transform->Rotation.Pitch = p_pitch;
	transform->Rotation.Yaw = p_yaw;
}

void GameObject::SetScale(float p_x, float p_y, float p_z) {
	transform->Scale.X = p_x;
	transform->Scale.Y = p_y;
	transform->Scale.Z = p_z;
}