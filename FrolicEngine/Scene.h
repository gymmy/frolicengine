#pragma once
#include "Object.h"
#include "GameObject.h"

class Scene : public GameObject {
protected:
	//	Scene children
	std::list<GameObject*> children;
public:
	//	Default Ctor
	explicit Scene();
	//	Name Scene Ctor
	explicit Scene(std::string p_name);
	//	Default Dtor
	~Scene();
	//	Add child to this scene
	void AddChild(GameObject* p_newChild);
};