#include "Object.h"

Object::Object() {
	std::cout << typeid(this).name() << std::endl;
}

Object::~Object() {
	std::cout << "object dtor called" << std::endl;
}

Object &Object::operator=(const Object& go) {
	std::cout << "Log: Object user defined copy assignment operator used." << std::endl;
	return *this;
}