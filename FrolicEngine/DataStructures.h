#pragma once

struct Vector2 {
public:
	float X = 0;
	float Y = 0;
	// Default Constructor
	explicit Vector2();
	// Assign member constructor
	explicit Vector2(float p_x, float p_y);
	// Destructor
	~Vector2();
	// Math operators
	Vector2 operator+(const Vector2 p_rhs);
	Vector2 operator+=(const Vector2 p_rhs);
	Vector2 operator-(const Vector2 p_rhs);
	Vector2 operator-=(const Vector2 p_rhs);
	Vector2 operator*(const float p_rhs);
	Vector2 operator*=(const float p_rhs);
	Vector2 operator/(const float p_rhs);
	Vector2 operator/=(const float p_rhs);
	Vector2 operator=(const Vector2 p_rhs);
};

struct Vector3 {	
public:
	float X;
	float Y;
	float Z;
	// Default Constructor
	explicit Vector3();
	// Assign member constructor
	explicit Vector3(float p_x, float p_y, float p_z);
	// Destructor
	~Vector3();
	// Math operators
	Vector3 operator+(const Vector3 p_rhs);
	Vector3 operator+=(const Vector3 p_rhs);
	Vector3 operator-(const Vector3 p_rhs);
	Vector3 operator-=(const Vector3 p_rhs);
	Vector3 operator*(const float p_rhs);
	Vector3 operator*=(const float p_rhs);
	Vector3 operator/(const float p_rhs);
	Vector3 operator/=(const float p_rhs);
	Vector3 operator=(const Vector3 p_rhs);
};

struct Quaternion {
public:
	// X
	float Roll = 0;
	// Y
	float Pitch = 0;
	// Z
	float Yaw = 0;
};