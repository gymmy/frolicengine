#pragma once
#include "DataStructures.h"
#include "Component.h"

class Transform final: public Component {
public: 
	//	Default Ctor
	explicit Transform();
	//	Default Dtor
	~Transform();
	Vector3 Location;
	Quaternion Rotation;
	Vector3 Scale;
	Transform *parent;
};