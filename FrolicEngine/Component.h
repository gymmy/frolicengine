#pragma once
#include "Object.h"
class Object;

class Component: public Object {
public:
	Component();
	~Component();
};