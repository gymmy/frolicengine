#include <iostream>
#include <memory>
#include "FrolicEngine.h"
using namespace std;

EngineMemorySystem MemoryManager;

void main() {
	MemoryManager.Initialize();
	shared_ptr<EngineEntity> sampleEntity = MemoryManager.CreateObject<EngineEntity>();
	cout << MemoryManager.GetActiveEntityCount() << endl;

	int stop;
	cin >> stop;

	MemoryManager.Shutdown();
}