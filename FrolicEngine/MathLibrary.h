#pragma once

#include "DataStructures.h"
#include <iostream>

namespace EngineMath {
	class MathLibrary {
	public:
		// Vector Mathematics
		float GetVectorMagnitude2D(const Vector2 p_lhs, const Vector2 p_rhs);
		float GetVectorMagnitude3D(const Vector3 p_lhs, const Vector3 p_rhs);

		// General Purpose
		float GetDotProduct();
		float GetCrossProduct();
	};
}