#pragma once

#include "EngineEntity.h"

class Object: public EngineEntity {
public:
	//	Default Ctor
	explicit Object();
	//	Default Dtor
	virtual ~Object();
	//	Copy assignment operator
	Object& operator=(const Object& o);
public:
	virtual void OnPostInitialize() {};
	virtual void OnBegin() {};
	virtual void Update() {};
	virtual void OnDestroy() {};
};