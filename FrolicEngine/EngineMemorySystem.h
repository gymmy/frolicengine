#pragma once

#include "EngineSubsystem.h"
#include "EngineEntity.h"
using namespace std;

/*
CLASS DESCRIPTION
-RAII Contruction and Destruction resource manager for engine entities.
*/

class EngineMemorySystem final : public EngineSubsystem{
private:
	list<shared_ptr<EngineEntity>> engineEntities;
public:
	//	Default Contructor (Empty)
	explicit EngineMemorySystem() {};
	//	Default Destructor (Empty)
	~EngineMemorySystem() {};
	//	Initialize System
	void Initialize() override;
	//	Shutdown System
	void Shutdown() override;
	//	Use this function to create objects so entity manager is notified.
	template<typename T>
	shared_ptr<T> CreateObject() {
		auto _obj = make_shared<T>();
		engineEntities.push_back(_obj);
		return _obj;
	}
	//	Get currently active entities
	size_t GetActiveEntityCount() { return engineEntities.size(); }
};