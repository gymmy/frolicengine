#pragma once

#include <iostream>
#include <string>
#include <list>
using namespace std;

/* 
CLASS DESCRIPTION
	-Is base class for every object the engine will create.
*/

class EngineEntity {
protected:
	unsigned int entityID;
public:
	//	Default Constructor
	explicit EngineEntity() : entityID(0){};
	//	Constructor: (int) EntityID
	explicit EngineEntity(int p_id) : entityID(p_id) {};
	//	Default Destructor
	virtual ~EngineEntity() {};

	void SetResourceID(int p_id) { entityID = p_id; }
	int GetResourceID() const { return entityID; }
};