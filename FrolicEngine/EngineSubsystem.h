#pragma once

#include <memory>
#include <iostream>

class EngineSubsystem {
public:
	explicit EngineSubsystem() {};
	virtual ~EngineSubsystem() {};
	//	Start up subsystem
	virtual void Initialize() = 0;
	//	Shut down subsystem
	virtual void Shutdown() = 0;
};